package br.com.galgo.cancelar;

import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.cancelamento.TelaCancelamentoExtrato;
import br.com.galgo.testes.recursos_comuns.pageObject.cancelamento.TelaCancelamentoPlCota;
import br.com.galgo.testes.recursos_comuns.pageObject.cancelamento.TelaCancelamentoPosicao;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;

public class CancelarInformacoes extends TelaGalgo {

	private Teste teste;
	private String caminhoMassaDadosFundos;

	public void cancelarInformacao(Teste teste) throws Exception {
		this.teste = teste;
		Ambiente ambiente = teste.getAmbiente();

		caminhoMassaDadosFundos = MassaDados.fromAmbiente(ambiente,
				ConstantesTestes.DESC_MASSA_DADOS_TRANSF).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		cancelarInformacao(usuario);
	}

	private void cancelarInformacao(Usuario usuario) throws Exception {
		Ambiente ambiente = teste.getAmbiente();
		Servico servico = teste.getServico();
		Canal canal = teste.getCanal();
		int qtdReteste = teste.getQtdReteste();

		int qtdExecucoes = 1;
		TelaGalgo.abrirBrowser(ambiente.getUrl());
		TelaLogin telaLogin = new TelaLogin();
		telaLogin.loginAs(usuario);

		while (qtdExecucoes <= qtdReteste) {
			String codigoSTI = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 1, qtdExecucoes);
			String dataBase = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 2, qtdExecucoes);

			String codigoSTICotista = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, false, 3, qtdExecucoes);

			cancelar(usuario, servico, canal, dataBase, codigoSTI,
					codigoSTICotista);
			qtdExecucoes++;
		}

	}

	private void cancelar(Usuario usuario, Servico servico, Canal canal,
			String dataBase, String codigoSTI, String codigoSTICotista)
			throws Exception {
		if (Servico.EXTRATO == servico) {
			cancelarExtrato(usuario, canal, dataBase, codigoSTI,
					codigoSTICotista);
		} else if (Servico.PL_COTA == servico || Servico.INFO_ANBIMA == servico) {
			cancelarPlCota(usuario, canal, dataBase, codigoSTI);
		} else if (Servico.POSICAO_ATIVOS == servico) {
			cancelarPosicao(usuario, dataBase, codigoSTI);
		}
	}

	private void cancelarExtrato(Usuario usuario, Canal canal,
			String dataFiltro, String codigoSTI, String codigoSTICotista)
			throws Exception {
		TelaHome telaHome = new TelaHome(usuario);
		TelaCancelamentoExtrato telaCancelamentoExtrato = (TelaCancelamentoExtrato) telaHome
				.acessarSubMenu(SubMenu.CANCELAMENTO_EXTRATO);
		if (Canal.PORTAL == canal) {
			telaCancelamentoExtrato.cancelar(dataFiltro, codigoSTI,
					codigoSTICotista);
		} else if (Canal.ARQUIVO == canal) {
			telaCancelamentoExtrato.cancelarArquivo(dataFiltro, codigoSTI,
					codigoSTICotista, usuario,
					ConstantesTestes.PATH_CANCELAMENTO_EXTRATO);
		}
	}

	private void cancelarPosicao(Usuario usuario, String dataFiltro,
			String codigoSTI) throws Exception {
		TelaHome telaHome = new TelaHome(usuario);
		TelaCancelamentoPosicao telaCancelamentoPosicao = (TelaCancelamentoPosicao) telaHome
				.acessarSubMenu(SubMenu.CANCELAMENTO_POSICAO);
		telaCancelamentoPosicao.clicarBotaoConcelarViaArquivo();
		telaCancelamentoPosicao.cancelarArquivo(dataFiltro, codigoSTI, usuario);
	}

	private void cancelarPlCota(Usuario usuario, Canal canal,
			String dataFiltro, String codigoSTI) throws Exception {
		TelaHome telaHome = new TelaHome(usuario);
		TelaCancelamentoPlCota TelaCancelamentoPlCota = (TelaCancelamentoPlCota) telaHome
				.acessarSubMenu(SubMenu.CANCELAMENTO_PL_COTA);
		if (Canal.PORTAL == canal) {
			TelaCancelamentoPlCota.cancelar(dataFiltro, codigoSTI);
		} else if (Canal.ARQUIVO == canal) {
			TelaCancelamentoPlCota.cancelarArquivo(dataFiltro, codigoSTI,
					usuario, ConstantesTestes.CAMINHO_XML_CANCELA_PL_COTA);
		}
	}

}