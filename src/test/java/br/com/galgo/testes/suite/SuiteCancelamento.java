package br.com.galgo.testes.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.cancelar.arquivo.CancelarExtratoArquivo;
import br.com.galgo.cancelar.arquivo.CancelarInformacaoAnbimaArquivo;
import br.com.galgo.cancelar.arquivo.CancelarPlCotaArquivo;
import br.com.galgo.cancelar.arquivo.CancelarPosicaoAtivosArquivo;
import br.com.galgo.cancelar.portal.CancelarExtratoPortal;
import br.com.galgo.cancelar.portal.CancelarInformacaoAnbimaPortal;
import br.com.galgo.cancelar.portal.CancelarPlCotaPortal;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ CancelarPlCotaPortal.class,//
	CancelarInformacaoAnbimaPortal.class,//
	CancelarExtratoPortal.class, //
	CancelarPlCotaArquivo.class,//
	CancelarInformacaoAnbimaArquivo.class,//
	CancelarExtratoArquivo.class,//
	CancelarPosicaoAtivosArquivo.class //
})
public class SuiteCancelamento {

}
